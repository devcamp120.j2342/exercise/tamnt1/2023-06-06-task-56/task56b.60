package com.example.customer.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.customer.Model.Visit;
import com.example.customer.Service.VisitService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class VisitController {
    @Autowired
    private VisitService service;

    @GetMapping("/visit")
    public List<Visit> getAccount() {
        return service.createVisit();

    }
}
