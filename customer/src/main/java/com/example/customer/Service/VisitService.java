package com.example.customer.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.customer.Model.Customer;
import com.example.customer.Model.Visit;

@Service
public class VisitService {
    public List<Visit> createVisit() {
        ArrayList<Visit> visitList = new ArrayList<>();
        Customer customer1 = new Customer("Tam");
        Customer customer2 = new Customer("Minh");
        Customer customer3 = new Customer("Khang");

        Visit visit1 = new Visit(customer1, new Date());
        Visit visit2 = new Visit(customer2, new Date());
        Visit visit3 = new Visit(customer3, new Date());
        visitList.addAll(Arrays.asList(visit1, visit2, visit3));
        return visitList;
    }
}
